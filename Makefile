CC=g++
CXX=g++ -std=c++11
CXXFLAGS=-I. -I/usr/include/Ice
LDLIBS=-lIce -lIceUtil -lpthread


APP=tinman
STUBS=$(addprefix $(APP), .h .cpp)

client: client.o $(APP).o

%.cpp %.h: %.ice
	slice2cpp $<
dist:
	mkdir dist

gen-dist: dist
	cp Client dist/
	icepatch2calc dist/

client.cpp: $(STUBS)

run-client: client
	./client "$(shell head -1 proxy.out)"

run-server:
	./server.py --Ice.Config=server.config | tee proxy.out

slice2cpp:
	slice2cpp --impl -I /usr/share/Ice-3.5.1/slice/ tinman.ice

clean:

	$(RM) *.o
