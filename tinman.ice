#include <Ice/BuiltinSequences.ice>

module Tinman {
  enum Action {AccelerateBegin, AccelerateEnd,
               BrakeBegin, BrakeEnd,
               TurnRight, TurnLeft, TurnEnd,
               UseNitro};
  sequence<Action> ActionSeq;

  struct Quaternion {
    float x;
    float y;
    float z;
    float w;
  };

  struct Vector3 {
    float x;
    float y;
    float z;
  };

  struct CarInfo {
    byte carid;
    Quaternion orientation;
    Vector3 position;
    Vector3 velocity;
    ActionSeq actions;
  };

  sequence<CarInfo> Snapshot;

  interface Player {
    void notify(Snapshot value);
    void start(Ice::StringSeq nicks);
  };

  interface AuthoritativeServer {
    void notify(Snapshot value);
    byte login(string name);
  };
};
