#!/usr/bin/python -u
# -*- coding:utf-8; tab-width:4; mode:python -*-


import sys
import Ice
Ice.loadSlice('tinman.ice -I/usr/share/Ice-3.5.1/slice')
import Tinman


class AuthoritativeServer(Tinman.AuthoritativeServer):

    def notify(self, value, current):
        print("size: ", value)
        for snapshot in value:
            print("ID: {0}: Orientation: {1} position: {2} velocity: {3} Action: {4}"
                  .format(snapshot.carid, snapshot.orientation, snapshot.position,
                          snapshot.velocity, snapshot.actions))
            sys.stdout.flush()

    def login(self, name, current):
        print("login: {0}".format(name))
        sys.stdout.flush()

        return 1


class Server(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = AuthoritativeServer()

        adapter = broker.createObjectAdapter("AuthoritativeServerAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("tinman"))

        print(proxy)
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


server = Server()
sys.exit(server.main(sys.argv))
