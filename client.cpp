// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <Ice/Ice.h>
#include "tinman.h"

using namespace Ice;
using namespace Tinman;

class Client: public Ice::Application {
  int run(int argc, char* argv[]) {
    ObjectPrx proxy = communicator()->stringToProxy(argv[1]);
   AuthoritativeServerPrx auth_server = AuthoritativeServerPrx::checkedCast(proxy);

   std::cout << "login: " << auth_server->login("Car1") << std::endl;

   CarInfo car_state = {::Ice::Byte(0), Quaternion{0, 0, 0, 1},
                        Vector3{0, 0, 0}, Vector3{1, 0, 1},
                        ActionSeq{AccelerateBegin, AccelerateEnd}};
   Snapshot snapshot = {car_state};

   auth_server->notify(snapshot);

    return 0;
  }
};

int main(int argc, char* argv[]) {
  Client app;
  return app.main(argc, argv);
}
